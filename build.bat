@echo off
REM     Build script.
REM     Usage: build.bat [debug|release|clean] [2017|2019]

setlocal

echo Build script started executing at %time% ...
REM Process command line arguments. Default is to build in release configuration.
set ProjectName=dx12_engine
set BuildType=%1
if "%BuildType%"=="" (set BuildType=release)

set MSVCVersion=%2
if "%MSVCVersion%"=="" (set MSVCVersion=2019)

echo Building %ProjectName% in %BuildType% configuration using MSVC %MSVCVersion%...

if %MSVCVersion%==2015 (
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
goto build_setup
)

if %MSVCVersion%==2017 (
call "%vs2017installdir%\VC\Auxiliary\Build\vcvarsall.bat" x64
goto build_setup
)

if %MSVCVersion%==2019 (
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
goto build_setup
)

echo Invalid MSVC compiler version specified.
goto error

:build_setup


REM     Make a build directory to store artifacts; remember, %~dp0 is just a special
REM     FOR variable reference in Windows that specifies the current directory the
REM     batch script is being run in.
set BuildDir=%~dp0msbuild

set ResourcesDir=%~dp0resource

if "%BuildType%"=="clean" (
    REM This allows execution of expressions at execution time instead of parse time, for user input
    setlocal EnableDelayedExpansion
    echo Cleaning build from directory: %BuildDir%. Files will be deleted^^!
    echo Continue ^(Y/N^)^?
    set /p ConfirmCleanBuild=
    if "!ConfirmCleanBuild!"=="Y" (
        echo Removing files in %BuildDir%...
        del /s /q %BuildDir%\*.*
    )
    goto end
)

echo Building in directory: %BuildDir% ...

if not exist %BuildDir% mkdir %BuildDir%

if %errorlevel% neq 0 goto error

pushd %BuildDir%

set EntryPoint=%~dp0src\%ProjectName%_main.cpp
set OutBin=%BuildDir%\%ProjectName%.exe
set IncludePaths=%~dp0%thirdparty
set ImGuiIncludePath=%IncludePaths%\imgui
set ImGuiImplPath=%ImGuiIncludePath%\backends

set ShadersPath=%~dp0resource\shaders
set VertexShaderPath=%ShadersPath%\SimpleVertexShader.hlsl
set PixelShaderPath=%ShadersPath%\SimplePixelShader.hlsl


REM Setup C/C++ compilation settings
set CommonLinkerFlags=/subsystem:windows /incremental:no /machine:x64 /nologo /defaultlib:Kernel32.lib /defaultlib:User32.lib /defaultlib:Gdi32.lib /defaultlib:d3d12.lib /defaultlib:d3dcompiler.lib /defaultlib:dxgi.lib /defaultlib:winmm.lib /defaultlib:Pathcch.lib /defaultlib:Shell32.lib
set DebugLinkerFlags=%CommonLinkerFlags% /opt:noref /debug /pdb:"%BuildDir%\%ProjectName%.pdb"
set ReleaseLinkerFlags=%CommonLinkerFlags% /opt:ref

set CommonCompilerFlags=/nologo /WX /W4 /EHsc /I "%IncludePaths%" /I "%ImGuiIncludePath%" /I "%ImGuiImplPath%"
set CompilerFlagsDebug=%CommonCompilerFlags% /Od /Zi /D_DEBUG
set CompilerFlagsRelease=%CommonCompilerFlags% /Ox /arch:AVX2 /DNDEBUG


if "%BuildType%"=="debug" (
   set BuildVertexShaderCmd=fxc /T:vs_5_1 /WX /E:main /Fd:"%BuildDir%\SimpleVertexShader.pdb" /Fo:"%BuildDir%\SimpleVertexShader.cso" /Zi /Od /nologo "%VertexShaderPath%"
   set BuildPixelShaderCmd=fxc /T:ps_5_1 /WX /E:main /Fd:"%BuildDir%\SimplePixelShader.pdb" /Fo:"%BuildDir%\SimplePixelShader.cso" /Zi /Od /nologo "%PixelShaderPath%"
   set BuildCommand=cl %CompilerFlagsDebug% "%EntryPoint%" /Fe:"%OutBin%" /link %DebugLinkerFlags%
) else (
   set BuildVertexShaderCmd=fxc /T:vs_5_1 /WX /E:main /Fo:"%BuildDir%\SimpleVertexShader.cso" /nologo /O1 /Qstrip_debug "%VertexShaderPath%"
   set BuildPixelShaderCmd=fxc /T:ps_5_1 /WX /E:main /Fo:"%BuildDir%\SimplePixelShader.cso" /nologo /O1 /Qstrip_debug "%PixelShaderPath%"
   set BuildCommand=cl %CompilerFlagsRelease% "%EntryPoint%" /Fe:"%OutBin%" /link %ReleaseLinkerFlags%
)

echo.
echo Compiling source files (command follows below)...
echo %BuildCommand%

echo.
echo Output from compilation:
%BuildCommand%

if %errorlevel% neq 0 goto error

echo.
echo Compiling shaders (commands follow below)...
echo %BuildVertexShaderCmd%
echo.
%BuildVertexShaderCmd%
if %errorlevel% neq 0 goto error

echo %BuildPixelShaderCmd%
echo.
%BuildPixelShaderCmd%
if %errorlevel% neq 0 goto error


REM Copy over any other resource files needed.
echo.
echo Copying resources to build directory...
xcopy /E /Y %ResourcesDir% %BuildDir%

if %errorlevel% neq 0 goto error
if %errorlevel% == 0 goto success

:error
echo.
echo ***************************************
echo *      !!! An error occurred!!!       *
echo ***************************************
goto end


:success
echo.
echo ***************************************
echo *    Build completed successfully.    *
echo ***************************************
goto end


:end
echo.
echo Build script finished execution at %time%.
popd

endlocal

exit /b %errorlevel%
