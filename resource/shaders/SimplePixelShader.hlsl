struct PxShaderInput
{
	float4 color : COLOR;
};


float4 main(PxShaderInput input) : SV_Target // NOTE: (sonictk) Semantic here identifies usage of the returned data.
{
	return input.color;
}
