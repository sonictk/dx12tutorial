struct VtxPosColor
{
	float3 pos : POSITION;
	float3 color: COLOR;
};


struct MVP
{
	matrix mvp;
};

// TODO: need a camera before we worry about MVP.
// NOTE: (sonictk) ConstantBuffer template construct is available from Shader Model 5.1 onwards.
// Enables support for descriptor arrays.
// ConstantBuffer<MVP> MVPCB : register(b0); // NOTE: (sonictk) b register type is reserved for cbuffer variables.


struct VtxShaderOut
{
	float4 color : COLOR;
	float4 pos : SV_POSITION;
};


VtxShaderOut main(VtxPosColor input)
{
	VtxShaderOut result;
	// result.pos = mul(MVPCB.mvp, float4(input.pos, 1.0f)); // NOTE: (sonictk) Get clip-space position.
	result.pos = float4(input.pos, 1.0f); // NOTE: (sonictk) Get clip-space position.
	result.color = float4(input.color, 1.0f);

	return result;
};
