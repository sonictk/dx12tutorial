#ifndef DX12_ENGINE_CONFIG_H
#define DX12_ENGINE_CONFIG_H


global_var int gWinWidth = 1280;
global_var int gWinHeight = 800;

global_var float gTargetFPS = 60.0f;

global_var float gCameraNearClip = 0.1f;
global_var float gCameraFarClip = 100.0f;

global_var float gCameraSpeed = gDefaultFlySpeed;
global_var float gCameraRollSpeed = 0.02f;
global_var float gMouseSens = 0.005f;
global_var float gCameraZoomSpeed = 2.0f;

global_var float gFOVDeg = 45.0f;

global_var bool gD3DUseWARPAdapter = false;
global_var int gD3DNumMSAASamples = 4; // NOTE: (sonictk) Set to 1 if you want to disable MSAA
global_var int gD3DMSAAQuality = 0; // NOTE: (sonictk) Used to store D3D12 supported quality levels.
global_var bool gVSync = true;
global_var bool gTearingSupported = false;
global_var bool gFullScreenMode = false;

global_var float gMasterVolume = 1.0f;

global_var bool gShowGUI = false;

#endif /* DX12_ENGINE_CONFIG_H */
