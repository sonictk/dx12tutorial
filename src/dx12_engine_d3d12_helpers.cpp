#include "libd3d12_helpers.h"

#pragma comment(lib, "dxguid.lib") // NOTE: (sonictk) Needed for d3dx12.h and dxgidebug.h
#include <d3dx12.h> // NOTE: (sonictk) For UpdateSubresources()

#ifdef D3D12_DEBUG
#include <dxgidebug.h>

void D3D12ReportLiveObjects()
{
	// NOTE: (sonictk) The IID_PPV_ARGS macro is used to retrieve an interface pointer.
	// ID3D12DebugDevice *pDbgDevice;
	// if (SUCCEEDED(gD3D12Device->QueryInterface(IID_PPV_ARGS(&pDbgDevice)))) {
	// 	pDbgDevice->ReportLiveDeviceObjects(D3D12_RLDO_SUMMARY|D3D12_RLDO_DETAIL|D3D12_RLDO_IGNORE_INTERNAL);
	// 	pDbgDevice->Release();
	// }

	IDXGIDebug1 *pDXGIDebug = NULL;
	if (SUCCEEDED(DXGIGetDebugInterface1(0, IID_PPV_ARGS(&pDXGIDebug)))) {
		pDXGIDebug->ReportLiveObjects(DXGI_DEBUG_ALL, DXGI_DEBUG_RLO_FLAGS(DXGI_DEBUG_RLO_DETAIL|DXGI_DEBUG_RLO_IGNORE_INTERNAL));
		pDXGIDebug->Release();
	}
	return;
}

#else
void D3D12ReportLiveObjects() {}
#endif // D3D12_DEBUG


void D3D12InitializeContent(ID3D12Device2 *device)
{
	size_t quadVtxBufSize = sizeof(gQuadVerts);
	D3D12_RESOURCE_DESC quadVtxsDesc;
	InitializeD3D12BufferResourceDesc(&quadVtxsDesc, quadVtxBufSize);

	size_t triVtxBufSize = sizeof(gTriVerts);
	D3D12_RESOURCE_DESC triVtxsDesc;
	InitializeD3D12BufferResourceDesc(&triVtxsDesc, triVtxBufSize);

	HRESULT hr = CreateD3D12DefaultHeapForResource(device, &triVtxsDesc, &gD3D12VtxBuf, "Vertex buffer default heap");
	assert(hr == S_OK);

	// NOTE: (sonictk) Create upload heap for the CPU to write to it and then let the GPU read from it.
	// We will upload the vertex buffer using this heap to the default heap.
	ID3D12Resource *uploadHeap;

	// NOTE: (sonictk) Buffers should be aligned to 64 KB.
	//https://docs.microsoft.com/en-us/windows/win32/direct3d12/uploading-resources#buffer-alignment
	size_t minRequiredHeapSize = quadVtxBufSize + triVtxBufSize;
	size_t bufAlignmentSize = 64 * 1024;
	size_t uploadHeapSize = bufAlignmentSize;
	if (minRequiredHeapSize > bufAlignmentSize) {
		uploadHeapSize = (minRequiredHeapSize / bufAlignmentSize) + bufAlignmentSize;
	}

	// TODO: (sonictk) Future work: use one upload heap instead to upload both the buffers?
	// UINT8 *pUploadHeapDataBegin = NULL;
	// UINT8 *pUploadHeapDataCurrent = NULL;
	// UINT8 *pUploadHeapDataEnd = NULL;
	hr = CreateD3D12UploadHeapWithSize(device, uploadHeapSize, &uploadHeap, "Vertex buffer upload resource heap");
	// hr = CreateD3D12UploadHeapForResource(device, &triVtxsDesc, &uploadHeap, "Vertex buffer upload resource heap");
	assert(hr == S_OK);

	// NOTE: (sonictk) Now create a command with the command list to copy the data from the
	// upload heap (that CPU writes to) to the default heap on the GPU.
	D3D12_SUBRESOURCE_DATA vtxData;
	ZeroMemory(&vtxData, sizeof(vtxData));
	vtxData.pData = (BYTE *)gTriVerts;
	vtxData.RowPitch = triVtxBufSize;
	vtxData.SlicePitch = triVtxBufSize;

	ID3D12CommandAllocator *cmdAlloc = CreateD3D12CommandAllocator(device, D3D12_COMMAND_LIST_TYPE_DIRECT);
	cmdAlloc->Reset();
	ID3D12GraphicsCommandList1 *cmdList = CreateD3D12GraphicsCommandList(device, cmdAlloc, D3D12_COMMAND_LIST_TYPE_DIRECT);
	cmdList->Reset(cmdAlloc, NULL);
	// TODO: (sonictk) Find a way to avoid using this helper abstraction so that we can optimize the allocation of resources.
	// NOTE: (sonictk) This helper function creates a command with the specified command list
	// to copy the data from the upload heap to the default heap.
	UpdateSubresources(cmdList, gD3D12VtxBuf, uploadHeap, 0, 0, 1, &vtxData);

	// NOTE: (sonictk) Transition the vertex buffer data from copy destination state to vertex buffer state.
	D3D12_RESOURCE_BARRIER barrier;
	ZeroMemory(&barrier, sizeof(barrier));
	barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	barrier.Transition.pResource = gD3D12VtxBuf;
	barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
	cmdList->ResourceBarrier(1, &barrier);

	// NOTE: (sonictk) Execute the command list to upload the vertex data to the GPU.
	cmdList->Close();
	ID3D12CommandList *ppCmdLists[] = {cmdList};
	gD3D12CmdQueue->ExecuteCommandLists(1, ppCmdLists);

	// NOTE: (sonictk) Now signal the fence, otherwise the vertex buffer might not be uploaded
	// by the time drawing has started. OK to use the global fence value since we're talking about uint64_t,
	// and this is only called on initialization of mesh resources.
	uint64_t fenceVal = SignalD3D12Fence(gD3D12CmdQueue, gD3D12Fence, gD3D12FenceValue);

	// NOTE: (sonictk) Stall CPU from going any further until the vertex buffer is uploaded to the GPU.
	WaitForD3D12FenceValue(gD3D12Fence, fenceVal, gD3D12FenceEvent, INFINITE);

	D3D12_VERTEX_BUFFER_VIEW vtxBufView;
	ZeroMemory(&vtxBufView, sizeof(vtxBufView));
	vtxBufView.BufferLocation = gD3D12VtxBuf->GetGPUVirtualAddress();
	vtxBufView.StrideInBytes = (UINT)(sizeof(VtxPosColor));
	vtxBufView.SizeInBytes = (UINT)triVtxBufSize;

	gD3D12VtxBufView = vtxBufView;

	uploadHeap->Release();
	cmdList->Release();
	cmdAlloc->Release();

	return;
}


void UpdateD3D12RenderTargetViews(ID3D12Device2 *device, IDXGISwapChain4 *swapChain, ID3D12DescriptorHeap *descHeap, const int numBufs)
{
	UINT rtvDescSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = descHeap->GetCPUDescriptorHandleForHeapStart();

	for (int i=0; i < numBufs; ++i) {
		gD3D12RenderTargetViewDescs[i] = rtvHandle;
		rtvHandle.ptr += (SIZE_T)rtvDescSize;

		ID3D12Resource *backBuffer;
#ifdef _DEBUG
		HRESULT hr = swapChain->GetBuffer((UINT)i, IID_PPV_ARGS(&backBuffer));
		assert(hr == S_OK);
#else
		swapChain->GetBuffer((UINT)i, IID_PPV_ARGS(&backBuffer));
#endif // _DEBUG
		device->CreateRenderTargetView(backBuffer, NULL, gD3D12RenderTargetViewDescs[i]);
		gD3D12MainRenderTargetResources[i] = backBuffer;
	}

	return;
}


/// Creates the Root Signature and the Pipeline State Object.
void D3D12CreateDeviceObjects(ID3D12Device *device)
{
	assert(device != NULL);

	// NOTE: (sonictk) First create the root signature. This defines the parameters that are
	// passed to the shader pipeline. It's basically a parameter list of data that the shader
	// functions expect (which also implies that the shaders must be compatible with the root signature)
	D3D12_DESCRIPTOR_RANGE1 descRange;
	ZeroMemory(&descRange, sizeof(descRange));
	descRange.RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descRange.NumDescriptors = 1;
	descRange.BaseShaderRegister = 0;
	descRange.RegisterSpace = 0;
	descRange.Flags = D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC;
	descRange.OffsetInDescriptorsFromTableStart = 0;

	// NOTE: (sonictk) This test will only do vertex/pixel shaders for now. If I wanted more,
	// I'd have to be adding them here.
	D3D12_ROOT_PARAMETER1 rootParam[2] = {};
	rootParam[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParam[0].Constants.ShaderRegister = 0;
	rootParam[0].Constants.RegisterSpace = 0;
	rootParam[0].Constants.Num32BitValues = 16;
	rootParam[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_VERTEX;

	rootParam[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParam[1].DescriptorTable.NumDescriptorRanges = 1;
	rootParam[1].DescriptorTable.pDescriptorRanges = &descRange;
	rootParam[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	// NOTE: (sonictk) Define the texture sampler directly in the root signature since it
	// doesn't need a descriptor heap and takes up no space in the root signature itself.
	D3D12_STATIC_SAMPLER_DESC staticSampler;
	ZeroMemory(&staticSampler, sizeof(staticSampler));
	staticSampler.Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
	staticSampler.AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	staticSampler.AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	staticSampler.AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	staticSampler.MipLODBias = 0.0f;
	staticSampler.MaxAnisotropy = 0;
	staticSampler.ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	staticSampler.BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
	staticSampler.MinLOD = 0.0f;
	staticSampler.MaxLOD = 0.0f;
	staticSampler.ShaderRegister = 0;
	staticSampler.RegisterSpace = 0;
	staticSampler.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	D3D12_ROOT_SIGNATURE_DESC1 rootSigDesc;
	rootSigDesc.NumParameters = _countof(rootParam);
	rootSigDesc.pParameters = rootParam;
	rootSigDesc.NumStaticSamplers = 1;
	rootSigDesc.pStaticSamplers = &staticSampler;
	rootSigDesc.Flags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT|
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS|
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS|
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;

	D3D12_VERSIONED_ROOT_SIGNATURE_DESC vRootSigDesc;
	vRootSigDesc.Version = D3D_ROOT_SIGNATURE_VERSION_1_1;
	vRootSigDesc.Desc_1_1 = rootSigDesc;

	ID3DBlob *pVtxShaderBlob = NULL;
	// TODO: (sonictk) Should probably compile shaders into exe itself or separate binary
	// and read from there instead. Make use of D3DCreateBlob() and memcpy the data over to
	// its buffer pointer for this.
	HRESULT hr = D3DReadFileToBlob(L"SimpleVertexShader.cso", &pVtxShaderBlob);
	assert(hr == S_OK);

	ID3DBlob *pPxShaderBlob = NULL;
	hr = D3DReadFileToBlob(L"SimplePixelShader.cso", &pPxShaderBlob);
	assert(hr == S_OK);

	ID3DBlob *pSerializedRootSig = NULL;
	ID3DBlob *pErrBlob = NULL;
	hr = D3D12SerializeVersionedRootSignature(&vRootSigDesc, &pSerializedRootSig, &pErrBlob);
	assert(hr == S_OK);
	hr = device->CreateRootSignature(0, pSerializedRootSig->GetBufferPointer(), pSerializedRootSig->GetBufferSize(), IID_PPV_ARGS(&gD3D12RootSignature));
	assert(hr == S_OK);
	pSerializedRootSig->Release();
	if (pErrBlob != NULL) { pErrBlob->Release(); }

	// NOTE: (sonictk) Now create the Pipeline State Object. It contains most of the state needed to
	// configure the graphics pipeline. If any of the state needs to change between draw calls (i.e.
	// need a different pixel shader or blend state) then a new PSO needs to be created.
	D3D12_BLEND_DESC blendStateDesc;
	ZeroMemory(&blendStateDesc, sizeof(blendStateDesc));
	blendStateDesc.AlphaToCoverageEnable = FALSE;
	blendStateDesc.IndependentBlendEnable = FALSE;
	blendStateDesc.RenderTarget[0].BlendEnable = TRUE;
	blendStateDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendStateDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendStateDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_INV_SRC_ALPHA;
	blendStateDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendStateDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendStateDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

	D3D12_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(rasterizerDesc));
	rasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	rasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
	rasterizerDesc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
	rasterizerDesc.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.ForcedSampleCount = 0;
	rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	D3D12_DEPTH_STENCIL_DESC depthStencilDesc;
	depthStencilDesc.DepthEnable = FALSE;
	depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.FrontFace.StencilFailOp = depthStencilDesc.FrontFace.StencilDepthFailOp = depthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	depthStencilDesc.BackFace = depthStencilDesc.FrontFace;

	// NOTE: (sonictk) Specifies the buffer format. (packed/interleaved)
	D3D12_INPUT_ELEMENT_DESC inputLayoutElemDesc[] = {
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
		{"COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0}
	};

	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;
	inputLayoutDesc.pInputElementDescs = inputLayoutElemDesc;
	inputLayoutDesc.NumElements = _countof(inputLayoutElemDesc);

	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc;
	ZeroMemory(&psoDesc, sizeof(psoDesc));
	psoDesc.pRootSignature = gD3D12RootSignature;
	psoDesc.VS.pShaderBytecode = pVtxShaderBlob->GetBufferPointer();
	psoDesc.VS.BytecodeLength = pVtxShaderBlob->GetBufferSize();
	psoDesc.PS.pShaderBytecode = pPxShaderBlob->GetBufferPointer();
	psoDesc.PS.BytecodeLength = pPxShaderBlob->GetBufferSize();
	psoDesc.BlendState = blendStateDesc;
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.RasterizerState = rasterizerDesc;
	psoDesc.DepthStencilState = depthStencilDesc;
	psoDesc.InputLayout = inputLayoutDesc;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = gD3D12RenderTargetViewFormat;
	psoDesc.SampleDesc.Count = 1;
	psoDesc.NodeMask = 0;
	psoDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;

	hr = device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&gD3D12PipelineState));
	assert(hr == S_OK);

	pVtxShaderBlob->Release();
	pPxShaderBlob->Release();

	return;
}


void D3D12InvalidateDeviceObjects()
{
	if (gD3D12PipelineState != NULL) { gD3D12PipelineState->Release(); }
	gD3D12PipelineState = NULL;

	if (gD3D12RootSignature != NULL) { gD3D12RootSignature->Release(); }
	gD3D12RootSignature = NULL;

	return;
}


int InitializeD3D12(const bool useWarp, HWND hWnd)
{
	assert(gMainWndHwnd != NULL);

	UINT createFactoryFlags = 0;

#ifdef D3D12_DEBUG
	createFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;

	ID3D12Debug *pD3D12Debug = NULL;
	// NOTE: (sonictk) The IID_PPV_ARGS macro is used to retrieve an interface pointer.
	if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&pD3D12Debug)))) {
		pD3D12Debug->EnableDebugLayer();
		pD3D12Debug->Release();
	}
#endif

	IDXGIFactory6 *pDXGIFactory6 = NULL;
	if (CreateDXGIFactory2(createFactoryFlags, IID_PPV_ARGS(&pDXGIFactory6)) != S_OK) {
		return -1;
	}

	// NOTE: (sonictk) Windows Advanced Rasterization Platform. Basically software rasterizer.
	// Check if we need to create this first, if not go ahead and make a normal D3D12 device.
	IDXGIAdapter4 *pDXGIAdapter4;
	if (useWarp) {
		if (pDXGIFactory6->EnumWarpAdapter(IID_PPV_ARGS(&pDXGIAdapter4)) != S_OK) {
			return -2;
		}
		// NOTE: (sonictk) Let's check if this WARP adapter can actually create a D3D12 device.
		if (SUCCEEDED(D3D12CreateDevice(pDXGIAdapter4, D3D_FEATURE_LEVEL_12_1, _uuidof(ID3D12Device), NULL))) {
#ifdef _DEBUG
			HRESULT hr = D3D12CreateDevice(pDXGIAdapter4, D3D_FEATURE_LEVEL_12_1, IID_PPV_ARGS(&gD3D12Device));
			assert(hr == S_OK);
#else
			D3D12CreateDevice(pDXGIAdapter4, D3D_FEATURE_LEVEL_12_1, IID_PPV_ARGS(&gD3D12Device));
#endif // _DEBUG
			SetD3D12ObjectName(gD3D12Device, "Global D3D12 WARP device");
		}
	} else {
		// NOTE: (sonictk) This is the "modern" way of finding the best GPU to use for our app.
		// In this case, we want the biggest, baddest GPU of the lot with the most amount of VRAM.
		// This will be at index 0,
		for (UINT i=0; pDXGIFactory6->EnumAdapterByGpuPreference(i, DXGI_GPU_PREFERENCE_HIGH_PERFORMANCE, IID_PPV_ARGS(&pDXGIAdapter4)) != DXGI_ERROR_NOT_FOUND; ++i) {

			// NOTE: (sonictk) Let's check if this adapter can actually create a D3D12 device.
			if (SUCCEEDED(D3D12CreateDevice(pDXGIAdapter4, D3D_FEATURE_LEVEL_12_1, _uuidof(ID3D12Device), NULL))) {
#ifdef _DEBUG
				HRESULT hr = D3D12CreateDevice(pDXGIAdapter4, D3D_FEATURE_LEVEL_12_1, IID_PPV_ARGS(&gD3D12Device));
				assert(hr == S_OK);
#else
				D3D12CreateDevice(pDXGIAdapter4, D3D_FEATURE_LEVEL_12_1, IID_PPV_ARGS(&gD3D12Device));
#endif // _DEBUG
				SetD3D12ObjectName(gD3D12Device, "Global D3D12 device");
				break;
			}
		}

#ifdef D3D12_DEBUG
		ID3D12InfoQueue *pD3D12InfoQueue = NULL;
		if (SUCCEEDED(gD3D12Device->QueryInterface(IID_PPV_ARGS(&pD3D12InfoQueue)))) {
			pD3D12InfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_CORRUPTION, TRUE); // NOTE: (sonictk) Memory corruption
			pD3D12InfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_ERROR, TRUE); // NOTE: (sonictk) This and warning are generated by the debug layer.
			pD3D12InfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_WARNING, TRUE);
		}

		// NOTE: (sonictk) Suppress D3D12 message categories here.
		// D3D12_MESSAGE_CATEGORY d3d12MsgCategories[] = {};
		D3D12_MESSAGE_SEVERITY d3d12MsgSeverities[] = {
			D3D12_MESSAGE_SEVERITY_INFO
		};

		// NOTE: (sonictk) Suppress individual messages by ID.
		D3D12_MESSAGE_ID d3d12MsgFilteredIds[] = {
			// D3D12_MESSAGE_ID_CLEARRENDERTARGETVIEW_MISMATCHINGCLEARVALUE, // NOTE: (sonictk) Occurs if render target is cleared using clear color that is not the optimized clear color specified during resource creation.
			D3D12_MESSAGE_ID_MAP_INVALID_NULLRANGE, // NOTE: (sonictk) Warning and following occur when a frame is captured using the graphics debugger in VS.
			D3D12_MESSAGE_ID_UNMAP_INVALID_NULLRANGE
		};

		D3D12_INFO_QUEUE_FILTER d3d12MsgQueueFilter;
		ZeroMemory(&d3d12MsgQueueFilter, sizeof(d3d12MsgQueueFilter));
		// d3d12MsgQueueFilter.DenyList.NumCategories = _countof(d3d12MsgCategories);
		// d3d12MsgQueueFilter.DenyList.pCategoryList = d3d12MsgCategories;
		d3d12MsgQueueFilter.DenyList.NumSeverities = _countof(d3d12MsgSeverities);
		d3d12MsgQueueFilter.DenyList.pSeverityList = d3d12MsgSeverities;
		d3d12MsgQueueFilter.DenyList.NumIDs = _countof(d3d12MsgFilteredIds);
		d3d12MsgQueueFilter.DenyList.pIDList = d3d12MsgFilteredIds;

		pD3D12InfoQueue->PushStorageFilter(&d3d12MsgQueueFilter);
		pD3D12InfoQueue->Release();
#endif
	}

	pDXGIAdapter4->Release();

	D3D12CreateDeviceObjects(gD3D12Device);

	// NOTE: (sonictk) Create an initial command queue to submit.
	gD3D12CmdQueue = CreateD3D12CmdQueue(gD3D12Device, D3D12_COMMAND_LIST_TYPE_DIRECT);
	SetD3D12ObjectName(gD3D12CmdQueue, "Global D3D12 command queue");

	// NOTE: (sonictk) Now create the swap chain.
	gD3D12SwapChain = CreateD3D12SwapChain(hWnd, gD3D12CmdQueue, gDefaultWindowWidth, gDefaultWindowHeight, gD3D12NumFramesInFlight, gD3D12RenderTargetViewFormat);

	gD3D12CurrentBackBufferIndex = gD3D12SwapChain->GetCurrentBackBufferIndex();

	// NOTE: (sonictk) Create a descriptor heap that describes the array of resource views.
	gD3D12RenderTargetViewDescHeap = CreateD3D12DescHeap(gD3D12Device, D3D12_DESCRIPTOR_HEAP_TYPE_RTV, gD3D12NumFramesInFlight, D3D12_DESCRIPTOR_HEAP_FLAG_NONE, 0);
	SetD3D12ObjectName(gD3D12RenderTargetViewDescHeap, "Global D3D12 RTV desc. heap");

	gD3D12ShaderResourceViewDescHeap = CreateD3D12DescHeap(gD3D12Device, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, gD3D12NumFramesInFlight, D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE, 0);
	SetD3D12ObjectName(gD3D12ShaderResourceViewDescHeap, "Global D3D12 SRV desc. heap");

	gD3D12RenderTargetViewDescSize = gD3D12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	UpdateD3D12RenderTargetViews(gD3D12Device, gD3D12SwapChain, gD3D12RenderTargetViewDescHeap, gD3D12NumFramesInFlight);

	// NOTE: (sonictk) Now create the command allocators and a command list.
	// There needs to be at least as many allocators as there are in-flight render frames.
	for (int i = 0; i < gD3D12NumFramesInFlight; ++i) {
		gD3D12CmdAllocs[i] = CreateD3D12CommandAllocator(gD3D12Device, D3D12_COMMAND_LIST_TYPE_DIRECT);
		SetD3D12ObjectName(gD3D12CmdAllocs[i], "Global D3D12 command allocator");
	}

	// NOTE: (sonictk) Only one list for now.
	gD3D12GfxCmdList = CreateD3D12GraphicsCommandList(gD3D12Device, gD3D12CmdAllocs[gD3D12CurrentBackBufferIndex], D3D12_COMMAND_LIST_TYPE_DIRECT);
	SetD3D12ObjectName(gD3D12GfxCmdList, "Global D3D12 command list");

	// NOTE: (sonictk) Create fence and fence event for synchronization. We'll block the CPU until the fence is signaled.
	gD3D12Fence = CreateD3D12Fence(gD3D12Device);
	SetD3D12ObjectName(gD3D12Fence, "Global D3D12 fence");

	gD3D12FenceEvent = CreateFenceEvent();

	pDXGIFactory6->Release();

	gD3D12Initialized = true;

	return 0;
}


void D3D12Render(IDXGISwapChain4 *swapChain)
{
	// NOTE: (sonictk) When we're using DXGI_SWAP_EFFECT_FLIP_DISCARD, the order of back buffer indices
	// is not guaranteed to be sequential. Thus we get the current index of the backbuffer here.
	UINT curBackBufIdx = swapChain->GetCurrentBackBufferIndex();

	// NOTE: (sonictk) Create ImGui state.
	D3D12CreateGUI();

	// NOTE: (sonictk) Before any commands can be recorded into the command list, the command allocator
	// and command list need to be both reset to initial states.
	ID3D12CommandAllocator *curCmdAlloc = gD3D12CmdAllocs[curBackBufIdx];
	HRESULT hr = curCmdAlloc->Reset();
	assert(hr == S_OK);
	hr = gD3D12GfxCmdList->Reset(curCmdAlloc, NULL);
	assert(hr == S_OK);

	// NOTE: (sonictk) Clear the render target.
	D3D12_RESOURCE_BARRIER barrier;
	ZeroMemory(&barrier, sizeof(barrier));
	barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	barrier.Transition.pResource = gD3D12MainRenderTargetResources[curBackBufIdx];
	barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	// NOTE: (sonictk) We have to manage tracking resource states ourselves.
	barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
	barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
	gD3D12GfxCmdList->ResourceBarrier(1, &barrier);

	// NOTE: (sonictk) Now clear the backbuffer.
	gD3D12GfxCmdList->ClearRenderTargetView(gD3D12RenderTargetViewDescs[curBackBufIdx], gD3D12ClearColor, 0, NULL);
	gD3D12GfxCmdList->OMSetRenderTargets(1, &gD3D12RenderTargetViewDescs[curBackBufIdx], FALSE, NULL);

	gD3D12GfxCmdList->SetDescriptorHeaps(1, &gD3D12ShaderResourceViewDescHeap);

	// NOTE: (sonictk) Now draw all meshes.
	gD3D12GfxCmdList->SetGraphicsRootSignature(gD3D12RootSignature);
	gD3D12GfxCmdList->SetPipelineState(gD3D12PipelineState);
	gD3D12GfxCmdList->RSSetViewports(1, &gD3D12Viewport);
	gD3D12GfxCmdList->RSSetScissorRects(1, &gD3D12ScissorRect);
	gD3D12GfxCmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	gD3D12GfxCmdList->IASetVertexBuffers(0, 1, &gD3D12VtxBufView);
	gD3D12GfxCmdList->DrawInstanced(3, 1, 0, 0);

	// NOTE: (sonictk) Draw all GUI elements through ImGui.
	D3D12DrawGUI();

	// NOTE: (sonictk) Render and update additional ImGui platform windows.
	// This is only relevant if ImGui's docking branch is being used.
	// D3D12UpdateGUIPlatformWindows();

	// NOTE: (sonictk) Transition resource back for swap chain presentation.
	barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
	barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
	gD3D12GfxCmdList->ResourceBarrier(1, &barrier);

	gD3D12GfxCmdList->Close();
	gD3D12CmdQueue->ExecuteCommandLists(1, (ID3D12CommandList *const *)&gD3D12GfxCmdList);

	UINT syncInterval = gVSync ? 1 : 0;
	UINT presentFlags = gTearingSupported && !gVSync ? DXGI_PRESENT_ALLOW_TEARING : 0;
	swapChain->Present(syncInterval, presentFlags);
	gD3D12FrameFenceValues[curBackBufIdx] = SignalD3D12Fence(gD3D12CmdQueue, gD3D12Fence, gD3D12FenceValue);
	// NOTE: (sonictk) We wait till we signal the fence until we update the global current backbuffer index.
	gD3D12CurrentBackBufferIndex = curBackBufIdx;
	// NOTE: (sonictk) Also stall the CPU thread before overwriting the contents of the current backbuffer
	// with the content of the next frame.
	WaitForD3D12FenceValue(gD3D12Fence, gD3D12FrameFenceValues[curBackBufIdx], gD3D12FenceEvent, INFINITE);

	return;
}


void D3D12Resize(IDXGISwapChain4 *swapChain, const uint32_t width, const uint32_t height)
{
	// NOTE: (sonictk) We don't need to account for no-effect resize operations here,
	// since the global client width/height is updated on every WM_SIZE event.
	ImGui_ImplDX12_InvalidateDeviceObjects();

	// NOTE: (sonictk) Don't allow 0 or negative size swap chain back buffers.
	gWinWidth = width < 1 ? 1 : width;
	gWinHeight = height < 1 ? 1 : height;

	// NOTE: (sonictk) Set the scissor rect and the viewport sizes.
	// TODO: (sonictk) See if this is better or just set the size to LONG_MAX to cover the
	// entire screen so that we don't need to update the scissor rectangle.
	gD3D12ScissorRect.left = 0;
	gD3D12ScissorRect.top = 0;
	gD3D12ScissorRect.right = width;
	gD3D12ScissorRect.bottom = height;

	gD3D12Viewport.TopLeftX = 0.0f;
	gD3D12Viewport.TopLeftY = 0.0f;
	gD3D12Viewport.Width = (FLOAT)width;
	gD3D12Viewport.Height = (FLOAT)height;
	gD3D12Viewport.MinDepth = 0.0f;
	gD3D12Viewport.MaxDepth = 1.0f;

	// NOTE: (sonictk) Flush the GPU queue to ensure the swap chain's back buffers
	// are not being referenced by an in-flight command list.
	FlushD3D12CommandQueue(gD3D12CmdQueue, gD3D12Fence, gD3D12FenceValue, gD3D12FenceEvent);

	for (int i=0; i < gD3D12NumFramesInFlight; ++i) {
		// NOTE: (sonictk) Any references to the back buffers must be released before
		// the swap chain can be resized.
		// TODO: (sonictk) Why is the ref count so hard to keep track of here? It seems to be increasing based on dependencies as well.
		gD3D12MainRenderTargetResources[i]->AddRef();
		ULONG refCount = gD3D12MainRenderTargetResources[i]->Release();
		for (ULONG j=refCount; j > 0; --j) {
			gD3D12MainRenderTargetResources[i]->Release();
		}

		gD3D12FrameFenceValues[i] = gD3D12FrameFenceValues[gD3D12CurrentBackBufferIndex];
	}

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	HRESULT hr = swapChain->GetDesc(&swapChainDesc);
	assert(hr == S_OK);
	hr = swapChain->ResizeBuffers(gD3D12NumFramesInFlight, gWinWidth, gWinHeight, swapChainDesc.BufferDesc.Format, swapChainDesc.Flags);
	assert(hr == S_OK);

	// NOTE: (sonictk) When we're using DXGI_SWAP_EFFECT_FLIP_DISCARD, the order of back buffer indices
	// is not guaranteed to be sequential. Thus we get the current index of the backbuffer here.
	UINT curBackBufIdx = swapChain->GetCurrentBackBufferIndex();
	gD3D12CurrentBackBufferIndex = curBackBufIdx;
	UpdateD3D12RenderTargetViews(gD3D12Device, swapChain, gD3D12RenderTargetViewDescHeap, gD3D12NumFramesInFlight);

	ImGui_ImplDX12_CreateDeviceObjects();

	return;
}


void SetFullscreenBorderless(bool enable)
{
	// TODO: (sonictk) For some reason, if the window is moved, the first alt-enter
	// will invoke the default Windows handler to do so, but the second time it is invoked,
	// the resize behaviour works correctly.
	if (gFullScreenMode != enable) {
		gFullScreenMode = enable;

		if (gFullScreenMode) {
			// NOTE: (sonictk) Store current window dimensions so that we can restore them.
#ifdef _DEBUG
			BOOL stat = GetWindowRect(gMainWndHwnd, &gWndRect);
			assert(stat != 0);
#else
			GetWindowRect(gMainWndHwnd, &gWndRect);
#endif // _DEBUG

			LONG wndStyle = WS_OVERLAPPEDWINDOW & ~(WS_CAPTION|WS_SYSMENU|WS_THICKFRAME|WS_MINIMIZEBOX|WS_MAXIMIZEBOX);
			LONG exWndStyle = WS_EX_OVERLAPPEDWINDOW & ~(WS_EX_WINDOWEDGE|WS_EX_CLIENTEDGE);
#ifdef _DEBUG
			LONG_PTR lpStat = SetWindowLongPtr(gMainWndHwnd, GWL_STYLE, (LONG_PTR)wndStyle);
			assert(lpStat != 0);
			lpStat = SetWindowLongPtr(gMainWndHwnd, GWL_EXSTYLE, (LONG_PTR)exWndStyle);
			assert(lpStat != 0);
#else
			SetWindowLongPtr(gMainWndHwnd, GWL_STYLE, (LONG_PTR)wndStyle);
			SetWindowLongPtr(gMainWndHwnd, GWL_EXSTYLE, (LONG_PTR)exWndStyle);
#endif // _DEBUG

			HMONITOR hMonitor = MonitorFromWindow(gMainWndHwnd, MONITOR_DEFAULTTONEAREST);
			MONITORINFOEX monitorInfo;
			ZeroMemory(&monitorInfo, sizeof(monitorInfo));
			monitorInfo.cbSize = sizeof(monitorInfo);
			BOOL bStat = GetMonitorInfo(hMonitor, &monitorInfo);
			assert(bStat != 0);

			bStat = SetWindowPos(gMainWndHwnd,
								 HWND_TOP,
								 monitorInfo.rcMonitor.left,
								 monitorInfo.rcMonitor.top,
								 monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left,
								 monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top,
								 SWP_FRAMECHANGED|SWP_NOACTIVATE);
			assert(bStat != 0);

			ShowWindow(gMainWndHwnd, SW_MAXIMIZE);
		} else {
			UINT wndStyle = WS_OVERLAPPEDWINDOW;
			UINT exWndStyle = WS_EX_OVERLAPPEDWINDOW;
#ifdef _DEBUG
			LONG_PTR lpStat = SetWindowLongPtr(gMainWndHwnd, GWL_STYLE, (LONG_PTR)wndStyle);
			assert(lpStat != 0);
			lpStat = SetWindowLongPtr(gMainWndHwnd, GWL_EXSTYLE, (LONG_PTR)exWndStyle);
			assert(lpStat != 0);
#else
			SetWindowLongPtr(gMainWndHwnd, GWL_STYLE, (LONG_PTR)wndStyle);
			SetWindowLongPtr(gMainWndHwnd, GWL_EXSTYLE, (LONG_PTR)exWndStyle);
#endif // _DEBUG
			SetWindowPos(gMainWndHwnd,
						 HWND_NOTOPMOST,
						 gWndRect.left,
						 gWndRect.top,
						 gWndRect.right - gWndRect.left,
						 gWndRect.bottom - gWndRect.top,
						 SWP_FRAMECHANGED|SWP_NOACTIVATE);
			ShowWindow(gMainWndHwnd, SW_NORMAL);
		}
	}
	return;
}


void UninitializeD3D12()
{
	// NOTE: (sonictk) Make sure that any resources that are in-flight on the GPU have
	// finished processing before they are released. Must do this before the app exits.
	FlushD3D12CommandQueue(gD3D12CmdQueue, gD3D12Fence, gD3D12FenceValue, gD3D12FenceEvent);

	for (int i=0; i < gD3D12NumFramesInFlight; ++i) {
		if (gD3D12CmdAllocs[i] != NULL) {
			gD3D12CmdAllocs[i]->Release();
			gD3D12CmdAllocs[i] = NULL;
		}
	}

	if (gD3D12VtxBuf != NULL) { gD3D12VtxBuf->Release(); }
	gD3D12VtxBuf = NULL;

	D3D12InvalidateDeviceObjects();

	if (gD3D12FenceEvent != NULL) { CloseHandle(gD3D12FenceEvent); }
	gD3D12FenceEvent = NULL;

	if (gD3D12Fence != NULL) { gD3D12Fence->Release(); }
	gD3D12Fence = NULL;

	if (gD3D12RenderTargetViewDescHeap != NULL) { gD3D12RenderTargetViewDescHeap->Release(); }
	gD3D12RenderTargetViewDescHeap = NULL;

	if (gD3D12ShaderResourceViewDescHeap != NULL) { gD3D12ShaderResourceViewDescHeap->Release(); }
	gD3D12ShaderResourceViewDescHeap = NULL;

	if (gD3D12SwapChain != NULL) { gD3D12SwapChain->Release(); }
	gD3D12SwapChain = NULL;

	if (gD3D12GfxCmdList != NULL) { gD3D12GfxCmdList->Release(); }
	gD3D12GfxCmdList = NULL;

	if (gD3D12CmdQueue != NULL) { gD3D12CmdQueue->Release(); }
	gD3D12CmdQueue = NULL;

	if (gD3D12Device != NULL) { gD3D12Device->Release(); }
	gD3D12Device = NULL;

	// D3D12ReportLiveObjects();

	gD3D12Initialized = false;

	return;
}
