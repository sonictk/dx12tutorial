#ifndef DX12_ENGINE_DEFS_H
#define DX12_ENGINE_DEFS_H

#ifdef _DEBUG
#ifndef D3D12_DEBUG
#define D3D12_DEBUG
#endif // D3D12_DEBUG
#endif // _DEBUG

#define global_var static
#define intern_var static

#include <DirectXMath.h>

namespace dx = DirectX;

global_var const int gMinWindowWidth = 640;
global_var const int gMinWindowHeight = 480;

global_var const int gMouseDeltaThreshold = 1;

global_var const int gDefaultWindowWidth = 1280;
global_var const int gDefaultWindowHeight = 800;

global_var const float gCameraDefaultNearClip = 0.1f;
global_var const float gCameraDefaultFarClip = 100.0f;

global_var const float gDefaultFlySpeed = 0.1f;
global_var const float gDefaultMouseSens = 0.5f;
global_var const float gDefaultCameraZoomSpeed = 2.0f;
global_var const float gDefaultFOV = 45.0f;


global_var const int gD3D12NumFramesInFlight = 3;


#endif /* DX12_ENGINE_DEFS_H */
