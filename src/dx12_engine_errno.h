#ifndef DX12_ENGINE_ERRNO_H
#define DX12_ENGINE_ERRNO_H

#include <limits.h>


enum D3D12EngineErr
{
	D3D12EngineErr_Unknown = INT_MIN,
	D3D12EngineErr_SwapChainInitFail,
	D3D12EngineErr_WindowUpdateFail,

	D3D12EngineErr_Success = 0
};

#endif /* DX12_ENGINE_ERRNO_H */
