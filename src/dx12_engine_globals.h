#ifndef DX12_ENGINE_GLOBALS_H
#define DX12_ENGINE_GLOBALS_H

#include <stdint.h>


#define EngineWndState_Minimized           0x1
#define EngineWndState_Maximized           0x2
#define EngineWndState_WindowedFullScreen  0x4
#define EngineWndState_Windowed            0x8
#define EngineWndState_Resizing            0x10


enum EngineState
{
	EngineState_Running,
	EngineState_Paused,
	EngineState_GameplayPaused,
	EngineState_Shutdown,

	EngineState_NumStates
};


global_var EngineState gEngineState = EngineState_Running;
global_var int gEngineWndState = EngineWndState_Windowed;

global_var bool gUseSoftwareRendering = false;


global_var HANDLE gProcessHeap = NULL;
global_var HWND gMainWndHwnd = NULL;

global_var RECT gWndRect;

global_var ID3D12Device2 *gD3D12Device = NULL;
global_var ID3D12CommandQueue *gD3D12CmdQueue = NULL;
global_var ID3D12GraphicsCommandList1 *gD3D12GfxCmdList = NULL;
global_var IDXGISwapChain4 *gD3D12SwapChain = NULL;
global_var ID3D12DescriptorHeap *gD3D12RenderTargetViewDescHeap = NULL; // Desc. heap for global render target view.
global_var ID3D12DescriptorHeap *gD3D12ShaderResourceViewDescHeap = NULL;
global_var ID3D12CommandAllocator *gD3D12CmdAllocs[gD3D12NumFramesInFlight] = {};
global_var ID3D12Resource *gD3D12MainRenderTargetResources[gD3D12NumFramesInFlight] = {};
global_var D3D12_CPU_DESCRIPTOR_HANDLE gD3D12RenderTargetViewDescs[gD3D12NumFramesInFlight] = {};
global_var ID3D12Fence *gD3D12Fence = NULL;

global_var uint64_t gD3D12FenceValue = 0;
global_var uint64_t gD3D12FrameFenceValues[gD3D12NumFramesInFlight] = {};
global_var HANDLE gD3D12FenceEvent = NULL;

global_var UINT gD3D12RenderTargetViewDescSize = 0;
global_var UINT gD3D12CurrentBackBufferIndex = 0;

global_var FLOAT gD3D12ClearColor[] = {0.4f, 0.6f, 0.9f, 1.0f};

global_var bool gD3D12Initialized = false;

global_var ID3D12RootSignature *gD3D12RootSignature = NULL;
global_var ID3D12PipelineState *gD3D12PipelineState = NULL;

// NOTE: (sonictk) Used for initializing the rasterizer stage of the rendering pipeline.
global_var D3D12_VIEWPORT gD3D12Viewport;

// NOTE: (sonictk) Scissor rect is used to mask out a rectangular region of the screen
// that will be allowed for rendering.
global_var D3D12_RECT gD3D12ScissorRect;

global_var DXGI_FORMAT gD3D12RenderTargetViewFormat = DXGI_FORMAT_R8G8B8A8_UNORM;

global_var int gImGuiWndWidth = 0;
global_var int gImGuiWndHeight = 0;
global_var int gImGuiWndPosX = 50;
global_var int gImGuiWndPosY = 50;

global_var bool gEngineExitRequested = false;
global_var bool gEngineHelpRequested = false;

#endif /* DX12_ENGINE_GLOBALS_H */
