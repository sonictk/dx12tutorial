#include "libd3d12_helpers.h"

void D3D12SetupGUI()
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO &imGuiIO = ImGui::GetIO();
	imGuiIO.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	ImGui::StyleColorsDark();

	ImGui_ImplWin32_Init(gMainWndHwnd);
	ImGui_ImplDX12_Init(gD3D12Device,
						gD3D12NumFramesInFlight,
						gD3D12RenderTargetViewFormat,
						gD3D12ShaderResourceViewDescHeap,
						gD3D12ShaderResourceViewDescHeap->GetCPUDescriptorHandleForHeapStart(),
						gD3D12ShaderResourceViewDescHeap->GetGPUDescriptorHandleForHeapStart());

	return;
}


void D3D12TeardownGUI()
{
	WaitForD3D12FenceValue(gD3D12Fence, gD3D12FenceValue, gD3D12FenceEvent, INFINITE);
	ImGui_ImplDX12_Shutdown(); // TODO: (sonictk) For some reason there is state that isn't unloaded
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	return;
}


void D3D12CreateGUI()
{
	ImGui_ImplDX12_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
	{
		// NOTE: (sonictk) Set initial size of ImGui window.
		ImVec2 dlgSize = ImVec2(0, 0); // NOTE: (sonictk) Setting to 0, 0 size will auto-resize to contents from second frame onwards.
		ImGui::SetNextWindowSize(ImVec2((float)gImGuiWndWidth, (float)gImGuiWndHeight), ImGuiCond_Always);

		ImVec2 dlgPos = ImVec2((float)gImGuiWndPosX, (float)gImGuiWndPosY);
		ImGui::SetNextWindowPos(dlgPos, ImGuiCond_Always);

		ImGui::Begin("Statistics", NULL, ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoCollapse|ImGuiWindowFlags_NoResize);
		if (gShowGUI) {
			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

			if (ImGui::Button("Hide")) {
				gShowGUI = false;
			}
		} else {
			if (ImGui::Button("Show statistics")) {
				gShowGUI = true;
			}
		}

		if (gEngineExitRequested == true) {
			ImGui::OpenPopup("Confirm exit?");
		}
		if (ImGui::BeginPopupModal("Confirm exit?", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
			// TODO: (sonictk) Pause gameplay but keep running rendering?
			ImGui::Text("Are you sure you want to exit? Any unsaved data will be lost.");
			if (ImGui::Button("Yes")) {
				gEngineState = EngineState_Shutdown;
			}
			ImGui::SetItemDefaultFocus();
			ImGui::SameLine();
			if (ImGui::Button("No")) {
				gEngineExitRequested = false;
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}

		if (gEngineHelpRequested == true) {
			ImGui::OpenPopup("Help");
		}
		if (ImGui::BeginPopupModal("Help", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
			ImGui::Text("This is the help documentation for the engine.\nPress F1 to view this help.");
			if (ImGui::Button("Close")) {
				gEngineHelpRequested = false;
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}

		ImGui::End();
	}

	return;
}


void D3D12DrawGUI()
{
	ImGui::Render();
	ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), gD3D12GfxCmdList);
	return;
}


// void D3D12UpdateGUIPlatformWindows()
// {
// 	ImGuiIO &io = ImGui::GetIO();
// 	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
// 		ImGui::UpdatePlatformWindows();
// 		ImGui::RenderPlatformWindowsDefault(NULL, (void *)gD3D12GfxCmdList);
// 	}

// 	return;
// }
