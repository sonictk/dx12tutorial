void OnKeyPress(const WPARAM btnState)
{
	bool altKeyDown = (GetAsyncKeyState(VK_MENU) & 0x8000) != 0;

	switch (btnState) {
	case VK_ESCAPE:
		gEngineExitRequested = true;
		break;
	case VK_RETURN:
		if (!altKeyDown) { break; }
	case VK_F11:
		SetFullscreenBorderless(!gFullScreenMode);
		break;
	default:
		break;
	}
	return;
}


void OnKeyRelease(const WPARAM btnState)
{
	switch (btnState) {
	case VK_F1:
		gEngineHelpRequested = true;
		break;
	default:
		break;
	}

	return;
}
