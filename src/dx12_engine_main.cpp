#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#include <Shellapi.h>

#include <assert.h>

#include <d3d12.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>

// #include <d3d12sdklayers.h>

// #include <DirectXMath.h> // NOTE: (sonictk) This _must_ be included _after_ ``windows.h`` according to MSDN.
// #include <DirectXColors.h>

#include "dx12_engine_defs.h"
#include "dx12_engine_errno.h"
#include "dx12_engine_strings.h"
#include "dx12_engine_config.h"
#include "dx12_engine_globals.h"

// NOTE: (sonictk) Using ImGui for user interface
#include <imgui.cpp> // NOTE: (sonictk) Must be included before ``imgui_internal.h``.
#include <imconfig.h>
#include <imgui_internal.h>
#include <imstb_rectpack.h>
#include <imstb_textedit.h>
#include <imstb_truetype.h>
#include <imgui_draw.cpp>
#include <imgui_widgets.cpp>
#include <imgui_tables.cpp>

#include <imgui_impl_win32.cpp>
#include <imgui_impl_dx12.cpp>

#include "libmath.cpp"
#include "libtimer_win32.cpp"

// NOTE: (sonictk) Internal only sources
#include "dx12_engine_tmp_globals.cpp"
#include "libd3d12_mesh.cpp"
#include "dx12_engine_gui.cpp"
#include "dx12_engine_d3d12_helpers.cpp"
#include "libd3d12_helpers.cpp"
#include "dx12_engine_input.cpp"


void OnWndResize()
{
	RECT clientRect;
	ZeroMemory(&clientRect, sizeof(clientRect));
	GetClientRect(gMainWndHwnd, &clientRect);

	int width = clientRect.right - clientRect.left;
	int height = clientRect.bottom - clientRect.top;

	D3D12Resize(gD3D12SwapChain, width, height);

	return;
}


LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	// NOTE: (sonictk) Ensure that swap chain, cmd list and allocators are all ready before
	// any resizing or rendering occurs.
	if (!gD3D12Initialized) {
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}

	// NOTE: (sonictk) This is for ImGui's own input handling to be enabled.
	if (ImGui_ImplWin32_WndProcHandler(hwnd, uMsg, wParam, lParam)) {
		return true;
	}

	switch(uMsg) {
	// NOTE: (sonictk) Sent whenever the window is activated/deactivated.
	case WM_ACTIVATE:
	{
		WORD lwParam = LOWORD(wParam);
		switch(lwParam) {
		case WA_INACTIVE:
			gEngineState = EngineState_Paused;
			break;
		case WA_ACTIVE:
			gEngineState = EngineState_Running;
			break;
		case WA_CLICKACTIVE:
		default:
			break;
		}
		return 0;
	}
	case WM_SIZE:
	{
		gWinWidth = (int)LOWORD(lParam);
		gWinHeight = (int)HIWORD(lParam);

		if (gD3D12Device != NULL) {
			switch(wParam) {
			case SIZE_MINIMIZED:
				gEngineState = EngineState_Paused;
				gEngineWndState |= EngineWndState_Minimized;
				gEngineWndState &= EngineWndState_WindowedFullScreen;
				gEngineWndState &= EngineWndState_Maximized;
				return 0;

			case SIZE_MAXIMIZED:
				gEngineState = EngineState_Running;
				gEngineWndState |= EngineWndState_Maximized;
				gEngineWndState &= EngineWndState_WindowedFullScreen;
				gEngineWndState &= EngineWndState_Minimized;
				break;

			case SIZE_RESTORED:
				if (gEngineWndState & EngineWndState_Resizing) {
					// NOTE: (sonictk) If a user is dragging the resize bars,
					// do not resize the buffers since ``WM_SIZE`` messages are
					// being sent continuously to the window. Instead, only resize
					// the buffers once the user releases the bars, which sends a
					// ``WM_EXITSIZEMOVE`` message.
					break;
				}
				if (gEngineWndState & EngineWndState_Minimized) {
					gEngineState = EngineState_Running;
					gEngineWndState |= EngineWndState_Maximized;
					gEngineWndState &= EngineWndState_WindowedFullScreen;
					gEngineWndState &= EngineWndState_Minimized;

				} else if (gEngineWndState & EngineWndState_Maximized) {
					gEngineState = EngineState_Paused;
					gEngineWndState |= EngineWndState_Minimized;
					gEngineWndState &= EngineWndState_WindowedFullScreen;
					gEngineWndState &= EngineWndState_Maximized;

				}
				OnWndResize();
				break;
			default:
				break;
			}
		}

		return 0;
	}
	// NOTE: (sonictk) Sent when the user is grabbing the resize handles.
	case WM_ENTERSIZEMOVE:
		gEngineState = EngineState_Paused;
		gEngineWndState |= EngineWndState_Resizing;
		return 0;

	// NOTE: (sonictk) Sent when the user releases the resize handles.
	case WM_EXITSIZEMOVE:
		OnWndResize();
		gEngineState = EngineState_Running;
		gEngineWndState &= EngineWndState_Resizing;

		return 0;

	case WM_PAINT:
		break;

	// NOTE: (sonictk) Sent when Alt-F4 is pressed or when Close button is pressed.
	case WM_CLOSE:
		// NOTE: (sonictk) If the user has already requested an exit (maybe by hitting Alt-F4 once),
		// we don't want to block them from force-quitting the application.
		if (gEngineExitRequested == true) {
			break;
		}
		gEngineExitRequested = true;
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_SYSCOMMAND:
		// NOTE: (sonictk) disables the ALT application menu
		if ((wParam & 0xfff0) == SC_KEYMENU) { return 0; }
		break;
	case WM_SYSKEYDOWN:
	case WM_KEYDOWN:
		OnKeyPress(wParam);
		break;

	case WM_KEYUP:
	case WM_SYSKEYUP:
		OnKeyRelease(wParam);
		break;

	// NOTE: (sonictk) This is sent when a menu is active and the user presses a
	// key that does not correspond to any mnemonic or accelerator.
	case WM_MENUCHAR:
		// NOTE: (sonictk) We do this to avoid the beep when hitting alt-enter.
		return MAKELRESULT(0, MNC_CLOSE);

	default:
		break;
	}

	// NOTE: (sonictk) As per MSDN guidance, forward all other unhandled messages
	// to the default window procedure to let Windows take care of it.
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}


int Run()
{
	queryTimer(&gTimerCount);

	bool bGotMsg;
	MSG msg = {0};
	msg.message = WM_NULL;

	// NOTE: (sonictk) According to MSDN: dispatches incoming sent messages, checks
	// the thread message queue for a posted message, and retrives the message (if any exist.)
	// Here, we say that we want no filtering of messages and to _not_ remove the
	// message from the queue after processing.
	// TODO: (sonictk) This first peek doesn't seem necessary...
	PeekMessage(&msg, NULL, 0U, 0U, PM_NOREMOVE);

	// TODO: (sonictk) Maybe set the main app thread using SetThreadAffinityMask()
	// so that we avoid this giving weird results even if there are bugs in the
	// BIOS or HAL?
	while (msg.message != WM_QUIT) {
		// NOTE: (sonictk) Process window events.
		// We use PeekMessage() so that we can use idle time to render the scene.
		// If we used GetMessage() instead, it would put the thread to sleep while
		// waiting for a message; we do not want this behavior. If there are no
		// Windows messages to be processed, we want to run our own rendering code instead.
		bGotMsg = (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE) != 0);
		if (bGotMsg) {
			TranslateMessage(&msg); // NOTE: (sonictk) Translates virtual key messages into character messages.
			DispatchMessage(&msg); // NOTE: (sonictk) Dispatch the translated message.

			continue;
		}

		// NOTE: (sonictk) No Windows messages remaining to be processed in the queue;
		// time to run our own rendering game code now!
		switch(gEngineState) {
		case EngineState_Shutdown:
			// NOTE: (sonictk) This sends ``WM_DESTROY`` and ``WM_NCDESTORY`` messages
			// to the window, which gets handled later on in the main window proc.
			DestroyWindow(gMainWndHwnd);
			break;
		case EngineState_Paused:
			Sleep(100);
			break;
		case EngineState_Running:
		{
			// NOTE: (sonictk) Make sure that this delta time is only used for game logic/physics;
			// should be completely independent of rendering framerate!
			float deltaTime = calcDeltaTime();
			global_var float gMaxTimeStep = 1.0f / gTargetFPS;
			deltaTime = deltaTime < gMaxTimeStep ? deltaTime : gMaxTimeStep;
		}
		default:
			UpdateD3D12RenderTargetViews(gD3D12Device, gD3D12SwapChain, gD3D12RenderTargetViewDescHeap, gD3D12NumFramesInFlight);
			D3D12Render(gD3D12SwapChain);
			break;
		}
	}

	return (int)msg.wParam; // NOTE: (sonictk) As guidance from MSDN dictates.
}


void ParseCmdLineArgs()
{
	int argc;
	wchar_t **argv = CommandLineToArgvW(GetCommandLineW(), &argc);
	for (int i=0; i < argc; ++i) {
		if (wcscmp(argv[i], DX12_ENGINE_CMDLINE_FLAG_WIDTH_SHORT_WIDE) == 0 || wcscmp(argv[i], DX12_ENGINE_CMDLINE_FLAG_WIDTH_WIDE)) {
			gWinWidth = wcstol(argv[++i], NULL, 10);
		}
	}

	return;
}


/**
 * Entry point for the application.
 *
 * @param hInstance 		The handle to the current instance of the application.
 * @param hPrevInstance 	Unused. Always ``NULL``. Signature required for Windows.
 * @param lpCmdLine 		The command line for the application.
 * @param nCmdShow 		Controls how the window is meant to be shown.
 *
 * @return					Exit code.
 */
int CALLBACK WinMain(HINSTANCE hInstance,
					 HINSTANCE,
					 LPSTR,
					 int nCmdShow)
{
	gProcessHeap = GetProcessHeap();
	if (gProcessHeap == NULL) {
		DWORD wErr = GetLastError();
		MessageBox(0, DX12_ENGINE_ERR_OUT_OF_MEMORY,0, 0);
		return (int)HRESULT_FROM_WIN32(wErr);
	}

	// Windows 10 Creators update adds Per Monitor V2 DPI awareness context.
	// Using this awareness context allows the client area of the window
	// to achieve 100% scaling while still allowing non-client window content to
	// be rendered in a DPI sensitive fashion.
	SetThreadDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);

	BOOL status;

	WNDCLASSEX wndClass;
	wndClass.cbSize = sizeof(WNDCLASSEX); // NOTE: (sonictk) This is what MSDN says to do
	wndClass.style = CS_HREDRAW|CS_VREDRAW; // NOTE: (sonictk) Redraws the entire window if the width/height of the client area is changed (where we are drawing d3d to.)
	wndClass.lpfnWndProc = WindowProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(0, IDI_APPLICATION); // NOTE: (sonictk) Default application icon
	wndClass.hCursor = LoadCursor(0, IDC_CROSS); // NOTE: (sonictk) Crosshair icon
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); // NOTE: (sonictk) Brush used for painting the background
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = DX12_ENGINE_WNDCLASS_NAME;
	wndClass.hIconSm = NULL; // NOTE: (sonictk) Small icon for the window class.

	ATOM result = RegisterClassEx(&wndClass);
	if (result == 0) {
		DWORD winError = GetLastError();
		switch(winError) {
		case ERROR_CLASS_ALREADY_EXISTS:
			MessageBox(0, DX12_ENGINE_ERR_CLASS_ALREADY_EXISTS, 0, 0);
			break;
		default:
			MessageBox(0, DX12_ENGINE_ERR_CLASS_REGISTRATION_FAIL, 0, 0);
			break;
		}

		return (int)HRESULT_FROM_WIN32(winError);
	}

	// NOTE: (sonictk) Because CreateWindow() also includes the menu bar and the
	// border in the width/height passed in, we get the _real_ dimensions that
	// should be passed in here by letting Windows calculate what they should be
	// so that we can have proper width/height for the client area we're drawing in.
	RECT clientRect = {0, 0, gWinWidth, gWinHeight};
	status = AdjustWindowRectEx(&clientRect, WS_OVERLAPPEDWINDOW, FALSE, WS_EX_OVERLAPPEDWINDOW); // NOTE: (sonictk) Use same style that we pass in later to CreateWindowEx().
	if (status == 0) {
		DWORD winError = GetLastError();
		MessageBox(0, DX12_ENGINE_ERR_CLIENT_DIMENSIONS_FAIL, 0, 0);

		return (int)HRESULT_FROM_WIN32(winError);
	}

	gMainWndHwnd = CreateWindowEx(WS_EX_OVERLAPPEDWINDOW,
								  DX12_ENGINE_WNDCLASS_NAME,
								  DX12_ENGINE_WND_TITLE,
								  WS_OVERLAPPEDWINDOW, // NOTE: (sonictk) includes title bar, window menu, sizing border, minimize/maximize buttons.
								  CW_USEDEFAULT, // NOTE: (sonictk) initial x pos
								  CW_USEDEFAULT, // NOTE: (sonictk) initial y pos
								  clientRect.right - clientRect.left,
								  clientRect.bottom - clientRect.top,
								  NULL, // NOTE: (sonictk) No parent window
								  NULL, // NOTE: (sonictk) No menu or child window
								  hInstance, // NOTE: (sonictk) Module associated with this window
								  NULL); // NOTE: (sonictk) No additional data needed

	if (gMainWndHwnd == NULL) {
		DWORD winError = GetLastError();
		MessageBox(0, DX12_ENGINE_ERR_WIN_CREATE_FAIL, 0, 0);

		return (int)HRESULT_FROM_WIN32(winError);
	}

	InitializeD3D12(gUseSoftwareRendering, gMainWndHwnd);
	D3D12InitializeContent(gD3D12Device);
	D3D12SetupGUI();

	// NOTE: (sonictk) Show the window using the flag specified by the program that
	// started the application, and send the app a WM_PAINT message.
	ShowWindow(gMainWndHwnd, nCmdShow);
	status = UpdateWindow(gMainWndHwnd);
	if (status == 0) {
		MessageBox(0, DX12_ENGINE_ERR_WIN_UPDATE_FAIL, 0, 0);

		return D3D12EngineErr_WindowUpdateFail;
	}

	// NOTE: (sonictk) Once the app has been created and initialized, enter the
	// message loop. Stay in this loop until a ``WM_QUIT`` message is received,
	// indicating that the app should be terminated.
	int exitCode = Run();

	D3D12TeardownGUI();
	UninitializeD3D12();

	DestroyWindow(gMainWndHwnd);
	UnregisterClass(DX12_ENGINE_WNDCLASS_NAME, wndClass.hInstance);

	return exitCode;
}
