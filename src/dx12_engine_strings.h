#ifndef DX12_ENGINE_STRINGS_H
#define DX12_ENGINE_STRINGS_H

#define DX12_ENGINE_ERR_UNSUPPORTED_PLATFORM "Unsupported platform."
#define DX12_ENGINE_WNDCLASS_NAME "D3D12WndClass"
#define DX12_ENGINE_WND_TITLE "Direct3D 12 Window"
#define DX12_ENGINE_TEXTURES_DIRECTORY_NAME "textures"
#define DX12_ENGINE_IMAGES_DIRECTORY_NAME "images"
#define DX12_ENGINE_AUDIO_DIRECTORY_NAME "audio"


#define DX12_ENGINE_ERR_OUT_OF_MEMORY "Out of memory!"
#define DX12_ENGINE_ERR_CLASS_ALREADY_EXISTS "Failed to register window class; it already exists!"
#define DX12_ENGINE_ERR_CLASS_REGISTRATION_FAIL "Failed to register window class; an unknown error occurred!"
#define DX12_ENGINE_ERR_CLIENT_DIMENSIONS_FAIL "Failed to calculate client dimensions; an unknown error occurred!"
#define DX12_ENGINE_ERR_WIN_CREATE_FAIL "Failed to create window!"
#define DX12_ENGINE_ERR_WIN_UPDATE_FAIL "Failed to update window!"
#define DX12_ENGINE_ERR_SWAPCHAIN_INIT_FAIL "Failed to initialize Direct3D swap chain!"


#define DX12_ENGINE_CMDLINE_FLAG_WIDTH_SHORT "-w"
#define DX12_ENGINE_CMDLINE_FLAG_WIDTH_SHORT_WIDE L"-w"

#define DX12_ENGINE_CMDLINE_FLAG_WIDTH "--width"
#define DX12_ENGINE_CMDLINE_FLAG_WIDTH_WIDE L"--width"

#define DX12_ENGINE_CMDLINE_FLAG_HEIGHT_SHORT "-h"
#define DX12_ENGINE_CMDLINE_FLAG_HEIGHT "--height"

#endif /* DX12_ENGINE_STRINGS_H */
