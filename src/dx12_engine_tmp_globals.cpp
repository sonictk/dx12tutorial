#include <DirectXMath.h>

namespace dx = DirectX;

global_var ID3D12Resource *gD3D12VtxBuf = NULL;
global_var D3D12_VERTEX_BUFFER_VIEW gD3D12VtxBufView = {};

global_var ID3D12Resource *gD3D12IdxBuf = NULL;
global_var D3D12_INDEX_BUFFER_VIEW *gD3D12IdxBufView = {};

// NOTE: (sonictk) Depth buffer storage.
global_var ID3D12Resource *gD3D12DepthBuf = NULL;
// NOTE: (sonictk) Descriptor heap for depth buffer.
global_var ID3D12DescriptorHeap *gD3D12DepthStencilViewHeap = NULL;

global_var dx::XMMATRIX gModelMat44 = {};
global_var dx::XMMATRIX gViewMat44 = {};
global_var dx::XMMATRIX gProjMat44 = {};

global_var bool isContentLoaded = false;
