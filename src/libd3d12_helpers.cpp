#include "libd3d12_helpers.h"


BOOL CheckD3D12ScreenTearingSupport(IDXGIFactory5 *pDXGIFactory)
{
	// NOTE: (sonictk) Check for screen tearing support to handle variable refresh rate displays.
	// TODO: (sonictk) MSDN says to do a bunch of weird things to use the older DXGI factory interfaces,
	// but it's not clear why I need to do that?
	BOOL screenTearingAllowed = FALSE;
	if (SUCCEEDED(pDXGIFactory->CheckFeatureSupport(DXGI_FEATURE_PRESENT_ALLOW_TEARING, &screenTearingAllowed, sizeof(screenTearingAllowed)))) {
		screenTearingAllowed = TRUE;
	}

	return screenTearingAllowed;
}


ID3D12CommandQueue *CreateD3D12CmdQueue(ID3D12Device *device, const D3D12_COMMAND_LIST_TYPE listType)
{
	ID3D12CommandQueue *pD3D12CmdQueue = NULL;
	D3D12_COMMAND_QUEUE_DESC cmdQueueDesc;
	ZeroMemory(&cmdQueueDesc, sizeof(cmdQueueDesc));
	cmdQueueDesc.Type = listType;
	cmdQueueDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
	cmdQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	cmdQueueDesc.NodeMask = 0;
	device->CreateCommandQueue(&cmdQueueDesc, IID_PPV_ARGS(&pD3D12CmdQueue));

	return pD3D12CmdQueue;
}


IDXGISwapChain4 *CreateD3D12SwapChain(HWND hWnd, ID3D12CommandQueue *pCmdQueue, const uint32_t width, const uint32_t height, const uint32_t bufCount, const DXGI_FORMAT rtvFormat)
{
	IDXGISwapChain4 *pSwapChain = NULL;
	IDXGIFactory5 *pDXGIFactory = NULL;

	UINT createFactoryFlags = 0;
#ifdef D3D12_DEBUG
	createFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
#endif
	HRESULT hr = CreateDXGIFactory2(createFactoryFlags, IID_PPV_ARGS(&pDXGIFactory));
	assert(hr == S_OK);

	DXGI_SWAP_CHAIN_DESC1 swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.Width = width;
	swapChainDesc.Height = height;
	swapChainDesc.Format = rtvFormat;
	swapChainDesc.Stereo = FALSE;
	swapChainDesc.SampleDesc.Count = 1; // NOTE: (sonictk) Must be specified as 1, 0 when using flip model swap chain, since we're not bitblting to the screen.
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = bufCount;
	swapChainDesc.Scaling = DXGI_SCALING_STRETCH;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD; // NOTE: (sonictk) Only vald flag when MSAA is enabled.
	swapChainDesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
	// NOTE: (sonictk) It's recommended to always allow tearing if tearing support is available.
	swapChainDesc.Flags = CheckD3D12ScreenTearingSupport(pDXGIFactory) ? DXGI_SWAP_CHAIN_FLAG_ALLOW_TEARING : 0;
	swapChainDesc.Flags |= DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	IDXGISwapChain1 *pSwapChain1 = NULL;
	hr = pDXGIFactory->CreateSwapChainForHwnd(pCmdQueue, hWnd, &swapChainDesc, NULL, NULL, &pSwapChain1);
	assert(hr == S_OK);

	pSwapChain1->QueryInterface(IID_PPV_ARGS(&pSwapChain));
	pSwapChain1->Release();

	// NOTE: (sonictk) Disable alt+enter fullscreen toggle feature. We'll handle switching to fullscreen ourselves.
	// TODO: (sonictk) This doesn't seem to actually affect the ability to do alt-enter.
	hr = pDXGIFactory->MakeWindowAssociation(hWnd, DXGI_MWA_NO_WINDOW_CHANGES|DXGI_MWA_NO_ALT_ENTER);
	assert(hr == S_OK);

	pDXGIFactory->Release();

	return pSwapChain;
}


ID3D12DescriptorHeap *CreateD3D12DescHeap(ID3D12Device2 *device, const D3D12_DESCRIPTOR_HEAP_TYPE hType, const uint32_t numDescs, const D3D12_DESCRIPTOR_HEAP_FLAGS flags, const UINT nodeMask)
{
	ID3D12DescriptorHeap *pD3D12DescHeap = NULL;
	D3D12_DESCRIPTOR_HEAP_DESC heapDesc;
	ZeroMemory(&heapDesc, sizeof(heapDesc));
	heapDesc.NumDescriptors = numDescs;
	heapDesc.Type = hType;
	heapDesc.Flags = flags;
	heapDesc.NodeMask = nodeMask;
#ifdef _DEBUG
	HRESULT hr = device->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&pD3D12DescHeap));
	assert(hr == S_OK);
#else
	device->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&pD3D12DescHeap));
#endif // _DEBUG

	return pD3D12DescHeap;
}


ID3D12CommandAllocator *CreateD3D12CommandAllocator(ID3D12Device2 *device, const D3D12_COMMAND_LIST_TYPE listType)
{
	ID3D12CommandAllocator *cmdAllocator = NULL;
#ifdef _DEBUG
	HRESULT hr = device->CreateCommandAllocator(listType, IID_PPV_ARGS(&cmdAllocator));
	assert(hr == S_OK);
#else
	device->CreateCommandAllocator(listType, IID_PPV_ARGS(&cmdAllocator));
#endif // _DEBUG

	return cmdAllocator;
}


ID3D12GraphicsCommandList1 *CreateD3D12GraphicsCommandList(ID3D12Device2 *device, ID3D12CommandAllocator *cmdAllocator, const D3D12_COMMAND_LIST_TYPE listType)
{
	ID3D12GraphicsCommandList1 *cmdList = NULL;
#ifdef _DEBUG
	HRESULT hr = device->CreateCommandList(0, listType, cmdAllocator, NULL, IID_PPV_ARGS(&cmdList));
	assert(hr == S_OK);
#else
	device->CreateCommandList(0, listType, cmdAllocator, NULL, IID_PPV_ARGS(&cmdList));
#endif // _DEBUG
	cmdList->Close();

	return cmdList;
}


ID3D12CommandList *CreateD3D12CommandList(ID3D12Device2 *device, ID3D12CommandAllocator *cmdAllocator, const D3D12_COMMAND_LIST_TYPE listType)
{
	ID3D12CommandList *cmdList = NULL;
#ifdef _DEBUG
	HRESULT hr = device->CreateCommandList(0, listType, cmdAllocator, NULL, IID_PPV_ARGS(&cmdList));
	assert(hr == S_OK);
#else
	device->CreateCommandList(0, listType, cmdAllocator, NULL, IID_PPV_ARGS(&cmdList));
#endif // _DEBUG

	return cmdList;
}


ID3D12Fence *CreateD3D12Fence(ID3D12Device2 *device)
{
	ID3D12Fence *fence = NULL;
#ifdef _DEBUG
	HRESULT hr = device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence));
	assert(hr == S_OK);
#else
	device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence));
#endif // _DEBUG
	return fence;
}


HANDLE CreateFenceEvent()
{
	HANDLE fenceEvent = NULL;
	fenceEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	assert(fenceEvent != NULL);

	return fenceEvent;
}


uint64_t SignalD3D12Fence(ID3D12CommandQueue *cmdQueue, ID3D12Fence *fence, uint64_t &fenceValue)
{
	uint64_t fenceValueForSignal = ++fenceValue;
	cmdQueue->Signal(fence, fenceValueForSignal);

	return fenceValueForSignal;
}


void WaitForD3D12FenceValue(ID3D12Fence *fence, const uint64_t fenceValue, HANDLE fenceEvent, const int duration)
{
	if (fence->GetCompletedValue() < fenceValue) {
#ifdef _DEBUG
		HRESULT hr = fence->SetEventOnCompletion(fenceValue, fenceEvent);
		assert(hr == S_OK);
#else
		fence->SetEventOnCompletion(fenceValue, fenceEvent);
#endif // _DEBUG
		WaitForSingleObject(fenceEvent, (DWORD)duration);
	}

	return;
}


void FlushD3D12CommandQueue(ID3D12CommandQueue *cmdQueue, ID3D12Fence *fence, uint64_t &fenceValue, HANDLE fenceEvent)
{
	uint64_t fenceValueForSignal = SignalD3D12Fence(cmdQueue, fence, fenceValue);
	WaitForD3D12FenceValue(fence, fenceValueForSignal, fenceEvent, INFINITE);
	return;
}


void InitializeD3D12BufferResourceDesc(D3D12_RESOURCE_DESC *desc, const size_t size)
{
	assert(desc != NULL);
	assert(size != 0);

	ZeroMemory(desc, sizeof(*desc));

	// NOTE: (sonictk) Basically the resource just needs to say "hey, I'm this size!"
	// So some of the fields in this struct are going to have odd values.
	desc->Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	desc->Alignment = 0;
	desc->Width = size;
	desc->Height = 1;
	desc->DepthOrArraySize = 1;
	desc->MipLevels = 1;
	desc->Format = DXGI_FORMAT_UNKNOWN;
	desc->SampleDesc.Count = 1;
	desc->SampleDesc.Quality = 0;
	desc->Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	desc->Flags = D3D12_RESOURCE_FLAG_NONE;

	return;
}


HRESULT CreateD3D12DefaultHeapForResource(ID3D12Device2 *device, const D3D12_RESOURCE_DESC *const resourceDesc, ID3D12Resource **resource, const char *debugName)
{
	assert(device != NULL);
	assert(resourceDesc != NULL);

	// NOTE: (sonictk) Reserve a resource on the GPU heap and commit it.
	/* This default heap is on the GPU and only the GPU has access to the memory. */
	D3D12_HEAP_PROPERTIES heapProps;
	heapProps.Type = D3D12_HEAP_TYPE_DEFAULT;
	heapProps.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	heapProps.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	heapProps.CreationNodeMask = 1; // NOTE: (sonictk) Single GPU adapter;
	heapProps.VisibleNodeMask = 0;

	HRESULT hr;
	hr = device->CreateCommittedResource(&heapProps,
										 D3D12_HEAP_FLAG_NONE,
										 resourceDesc,
										 D3D12_RESOURCE_STATE_COPY_DEST,
										 NULL,
										 IID_PPV_ARGS(resource));
	if (debugName != NULL) {
		SetD3D12ObjectName(*resource, debugName);
	}
	return hr;
}


HRESULT CreateD3D12UploadHeapForResource(ID3D12Device2 *device, const D3D12_RESOURCE_DESC *const resourceDesc, ID3D12Resource **uploadHeap, const char *debugName)
{
	D3D12_HEAP_PROPERTIES uploadHeapProps;
	uploadHeapProps.Type = D3D12_HEAP_TYPE_UPLOAD;
	uploadHeapProps.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	uploadHeapProps.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	uploadHeapProps.CreationNodeMask = 1;
	uploadHeapProps.VisibleNodeMask = 0;
	HRESULT hr = device->CreateCommittedResource(&uploadHeapProps,
												 D3D12_HEAP_FLAG_NONE,
												 resourceDesc,
												 D3D12_RESOURCE_STATE_GENERIC_READ,
												 NULL,
												 IID_PPV_ARGS(uploadHeap));
	if (debugName != NULL) {
		SetD3D12ObjectName(*uploadHeap, debugName);
	}
	return hr;
}


HRESULT CreateD3D12UploadHeapWithSize(ID3D12Device2 *device, const size_t size, ID3D12Resource **uploadHeap, const char *debugName)
{
	D3D12_RESOURCE_DESC resourceDesc;
	InitializeD3D12BufferResourceDesc(&resourceDesc, size);

	D3D12_HEAP_PROPERTIES uploadHeapProps;
	uploadHeapProps.Type = D3D12_HEAP_TYPE_UPLOAD;
	uploadHeapProps.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	uploadHeapProps.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	uploadHeapProps.CreationNodeMask = 1;
	uploadHeapProps.VisibleNodeMask = 0;
	HRESULT hr = device->CreateCommittedResource(&uploadHeapProps,
												 D3D12_HEAP_FLAG_NONE,
												 &resourceDesc,
												 D3D12_RESOURCE_STATE_GENERIC_READ,
												 NULL,
												 IID_PPV_ARGS(uploadHeap));
	if (debugName != NULL) {
		SetD3D12ObjectName(*uploadHeap, debugName);
	}

	return hr;
}


HRESULT InitializeD3D12UploadHeap(ID3D12Resource *uploadHeap)
{
	void *pData = NULL;
	D3D12_RANGE readRange;
	readRange.Begin = 0;
	readRange.End = 0;
	HRESULT hr = uploadHeap->Map(0, &readRange, &pData);
	return hr;
}


// HRESULT SetDataToD3D12UploadHeap(ID3D12Resource *uploadHeap, const void *pData, const size_t dataSize, const size_t numDatums, const size_t alignment, const size_t &byteOffset)
// {
// 	assert(uploadHeap != NULL);
// 	assert(pData != NULL);
// 	size_t totalDataSize = dataSize * numDatums;
// 	if (totalDataSize == 0) { return S_OK; }

// 	// TODO: (sonictk) finish this.
// 	HRESULT hr = Suballocate;

// 	return S_OK;
// }


#ifdef D3D12_DEBUG
void SetD3D12ObjectName(ID3D12Object *obj, const char *name)
{
	obj->SetPrivateData(WKPDID_D3DDebugObjectName, (UINT)(strlen(name) + 1), name);
	return;
}

void ClearD3D12ObjectName(ID3D12Object *obj)
{
	obj->SetPrivateData(WKPDID_D3DDebugObjectName, 0, NULL);
	return;
}
#else
void SetD3D12ObjectName(ID3D12Object *, const char *) {}
void ClearD3D12ObjectName(ID3D12Object *) {}
#endif // D3D12_DEBUG
