#ifndef LIBD3D12_HELPERS_H
#define LIBD3D12_HELPERS_H


BOOL CheckD3D12ScreenTearingSupport(IDXGIFactory5 *pDXGIFactory);


ID3D12CommandQueue *CreateD3D12CmdQueue(ID3D12Device *device, const D3D12_COMMAND_LIST_TYPE listType);


IDXGISwapChain4 *CreateD3D12SwapChain(HWND hWnd, ID3D12CommandQueue *pCmdQueue, const uint32_t width, const uint32_t height, const uint32_t bufCount, const DXGI_FORMAT rtvFormat);


ID3D12DescriptorHeap *CreateD3D12DescHeap(ID3D12Device2 *device, const D3D12_DESCRIPTOR_HEAP_TYPE hType, const uint32_t numDescs, const D3D12_DESCRIPTOR_HEAP_FLAGS flags, const UINT nodeMask);


ID3D12CommandAllocator *CreateD3D12CommandAllocator(ID3D12Device2 *device, const D3D12_COMMAND_LIST_TYPE listType);


ID3D12GraphicsCommandList1 *CreateD3D12GraphicsCommandList(ID3D12Device2 *device, ID3D12CommandAllocator *cmdAllocator, const D3D12_COMMAND_LIST_TYPE listType);


ID3D12CommandList *CreateD3D12CommandList(ID3D12Device2 *device, ID3D12CommandAllocator *cmdAllocator, const D3D12_COMMAND_LIST_TYPE listType);


ID3D12Fence *CreateD3D12Fence(ID3D12Device2 *device);


/// OS event is used to cause the CPU thread to stall using WaitForSingleObject
HANDLE CreateFenceEvent();


/// Signals the fence from the GPU. Fence is only signaled once the GPU command queue has reached the call Signal() udring execution.
/// Any commands queued before Signal() must complete execution before the fence will be signaled.
uint64_t SignalD3D12Fence(ID3D12CommandQueue *cmdQueue, ID3D12Fence *fence, uint64_t &fenceValue);


/// Used to stall the CPU thread if the fence has not yet been signaled with a specific value.
void WaitForD3D12FenceValue(ID3D12Fence *fence, const uint64_t fenceValue, HANDLE fenceEvent, const int duration);


/// Ensures that any commands previously executed on the GPU have finished executing before the CPU thread
/// is allowed to continue processing.
void FlushD3D12CommandQueue(ID3D12CommandQueue *cmdQueue, ID3D12Fence *fence, uint64_t &fenceValue, HANDLE fenceEvent);


/// Just initiallizes a resource descriptor of a certain size. (i.e. Used for vertex/index buffers.)
void InitializeD3D12BufferResourceDesc(D3D12_RESOURCE_DESC *desc, const size_t size);


/// Creates a resource and implicit heap on the GPU which is big enough to contain the entire resource
/// and the resource is mapped to the heap. Only GPU has access to this memory.
/// The resource is created in an initial state where it will be used as the destination for a future copy operation (write-only)/
HRESULT CreateD3D12DefaultHeapForResource(ID3D12Device2 *device, const D3D12_RESOURCE_DESC *const resourceDesc, ID3D12Resource **resource, const char *debugName);


/// Creates an upload heap for the CPU to write to it and then let the GPU read from it later on.
/// This heap will be big enough to contain the entire resource.
HRESULT CreateD3D12UploadHeapForResource(ID3D12Device2 *device, const D3D12_RESOURCE_DESC *const resourceDesc, ID3D12Resource **uploadHeap, const char *debugName);


HRESULT CreateD3D12UploadHeapWithSize(ID3D12Device2 *device, const size_t size, ID3D12Resource **uploadHeap, const char *debugName);


/**
 * This will initialize the upload heap so that the CPU has no read access to it,
 * only write access. It also keeps the buffer mapped in memory.
 *
 * @param uploadHeap	The upload heap/buffer to initialize.
 *
 * @return				A status code.
 */
HRESULT InitializeD3D12UploadHeap(ID3D12Resource *uploadHeap);


/// Allows setting of a name that can be seen in the DXGI validation layer when debugging.
void SetD3D12ObjectName(ID3D12Object *obj, const char *name);


/// Clears a previous debug name that was set using ``SetD3D12ObjectName()``.
void ClearD3D12ObjectName(ID3D12Object *obj);


#endif /* LIBD3D12_HELPERS_H */
