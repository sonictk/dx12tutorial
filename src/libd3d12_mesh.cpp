#include "libd3d12_mesh.h"


global_var VtxPosColor gCubeVerts[] = {
	{ dx::XMFLOAT3(-1.0f, -1.0f, -1.0f), dx::XMFLOAT3(0.0f, 0.0f, 0.0f) }, // 0
	{ dx::XMFLOAT3(-1.0f,  1.0f, -1.0f), dx::XMFLOAT3(0.0f, 1.0f, 0.0f) }, // 1
	{ dx::XMFLOAT3( 1.0f,  1.0f, -1.0f), dx::XMFLOAT3(1.0f, 1.0f, 0.0f) }, // 2
	{ dx::XMFLOAT3( 1.0f, -1.0f, -1.0f), dx::XMFLOAT3(1.0f, 0.0f, 0.0f) }, // 3
	{ dx::XMFLOAT3(-1.0f, -1.0f,  1.0f), dx::XMFLOAT3(0.0f, 0.0f, 1.0f) }, // 4
	{ dx::XMFLOAT3(-1.0f,  1.0f,  1.0f), dx::XMFLOAT3(0.0f, 1.0f, 1.0f) }, // 5
	{ dx::XMFLOAT3( 1.0f,  1.0f,  1.0f), dx::XMFLOAT3(1.0f, 1.0f, 1.0f) }, // 6
	{ dx::XMFLOAT3( 1.0f, -1.0f,  1.0f), dx::XMFLOAT3(1.0f, 0.0f, 1.0f) }  // 7
};

// NOTE: (sonictk) Default winding order is clockwise.
global_var int16_t gCubeIndices[] = {
    0, 1, 2, 0, 2, 3,
    4, 6, 5, 4, 7, 6,
    4, 5, 1, 4, 1, 0,
    3, 2, 6, 3, 6, 7,
    1, 5, 6, 1, 6, 2,
    4, 0, 3, 4, 3, 7
};


global_var VtxPosColor gTriVerts[] = {
	{ dx::XMFLOAT3( 0.0f,  0.5f,  0.0f), dx::XMFLOAT3(0.0f, 0.0f, 1.0f) }, // 0
	{ dx::XMFLOAT3( 0.5f, -0.5f,  0.0f), dx::XMFLOAT3(0.0f, 1.0f, 0.0f) }, // 1
	{ dx::XMFLOAT3(-0.5f, -0.5f,  0.0f), dx::XMFLOAT3(1.0f, 0.0f, 0.0f) }, // 2
};


global_var VtxPosColor gQuadVerts[] = {
	{ dx::XMFLOAT3(-0.5f,  0.5f,  0.0f), dx::XMFLOAT3(0.0f, 0.0f, 1.0f) }, // top left
	{ dx::XMFLOAT3( 0.5f, -0.5f,  0.0f), dx::XMFLOAT3(0.0f, 1.0f, 0.0f) }, // bot right
	{ dx::XMFLOAT3(-0.5f, -0.5f,  0.0f), dx::XMFLOAT3(1.0f, 0.0f, 0.0f) }, // bot left
	{ dx::XMFLOAT3( 0.5f,  0.5f,  0.0f), dx::XMFLOAT3(1.0f, 0.0f, 1.0f) }, // top right
};

global_var int16_t gQuadIndices[] = {
	0, 1, 2, // tri 1
	0, 3, 1  // tri 2
};
