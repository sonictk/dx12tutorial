#ifndef LIBD3D12_MESH_H
#define LIBD3D12_MESH_H


struct VtxPosColor
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT3 color;
};


#endif /* LIBD3D12_MESH_H */
