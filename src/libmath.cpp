#include "libmath.h"

int clamp(int val, int min, int max)
{
	int result = val;

	if (val < min) {
		result = min;
	} else if (val > max) {
		result = max;
	}

	return result;
}

unsigned int clamp(unsigned int val, unsigned int min, unsigned int max)
{
	unsigned int result = val;

	if (val < min) {
		result = min;
	} else if (val > max) {
		result = max;
	}

	return result;
}

float clamp(float val, float min, float max)
{
	float result = val;

	if (val < min) {
		result = min;
	} else if (val > max) {
		result = max;
	}

	return result;
}

double clamp(double val, double min, double max)
{
	double result = val;

	if (val < min) {
		result = min;
	} else if (val > max) {
		result = max;
	}

	return result;
}
