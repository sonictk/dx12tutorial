#ifndef LIBMATH_H
#define LIBMATH_H

int clamp(int val, int min, int max);

unsigned int clamp(unsigned int val, unsigned int min, unsigned int max);

float clamp(float val, float min, float max);

double clamp(double val, double min, double max);

#endif /* LIBMATH_H */
