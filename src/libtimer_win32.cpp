global_var LARGE_INTEGER gCPUTimerFreq = {0};
global_var LARGE_INTEGER gTimerCount = {0};


bool queryTimer(LARGE_INTEGER *lpPerformanceCount)
{
	BOOL stat = QueryPerformanceCounter(lpPerformanceCount);
	return (bool)stat;
}


float calcDeltaTime()
{
	LARGE_INTEGER curTime;
	queryTimer(&curTime);
	float deltaTime = (float)(curTime.QuadPart - gTimerCount.QuadPart) / (float)gCPUTimerFreq.QuadPart;
	gTimerCount = curTime;
	return deltaTime;
}
