# TODO list #

## General ##

[ ] Resources (textures, meshes, audio, etc.) compiler.


## Graphics ##

[ ] Look into index buffering the prims.
[ ] Upload multiple index/vertex buffers for the test content at a time.
[ ] Re-write UpdateSubresources() to avoid extra memcpys and get rid of helper abstraction in general.
[ ] Mesh importer.
[ ] Lighting.


## Camera ##

[ ] Camera system.


## Input ##

[ ] Input system.


## Audio ##

[ ] WASAPI layer
[ ] Positional audio
[ ] Mixer
